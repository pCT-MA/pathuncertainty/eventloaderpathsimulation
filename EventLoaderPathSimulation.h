#pragma once

#include <random>

#include "core/module/Module.hpp"
#include "objects/PhantomPath.hpp"

#include "SimulationParameters.hpp"

class TTree;
class TH2D;

namespace corryvreckan {

    class EventLoaderPathSimulation : public Module {
    public:
        EventLoaderPathSimulation(Configuration& config, std::vector<std::shared_ptr<Detector>> detectors);
        ~EventLoaderPathSimulation();

        void initialize() override;
        StatusCode run(const std::shared_ptr<Clipboard>& clipboard) override;
        void finalize(const std::shared_ptr<ReadonlyClipboard>& clipboard) override;

    private:
        /* Types used for strip detectors. Only the column of pixel hits is
        filled with data if such a type is used (row is kept at 0). */
        static constexpr const char* gStripXType = "strip_x";
        static constexpr const char* gStripUType = "strip_u";
        static constexpr const char* gStripVType = "strip_v";

        static constexpr const char* gTracingTreeName = "tracing";
        static constexpr const char* gEventBranchName = "event";
        static constexpr const char* gXBranchName = "x";
        static constexpr const char* gYBranchName = "y";
        static constexpr const char* gZBranchName = "z";
        static constexpr const char* gEnergyBranchName = "energy";
        static constexpr const char* gIsPhantomBranchName = "isPhantom";

        // config
        std::default_random_engine mRNG;
        std::string mInFileName;
        std::string mXmlConfig;
        bool mApplyErrors;

        // x, y, energy
        typedef std::array<double, 3> Measurement;
        std::vector<Measurement> mMeasurements;

        // temporaries for root
        TFile* mInFile;
        TTree* mTracingTree;
        Long64_t mCurrentIndex;
        Long64_t mNumberOfEntries;
        Int_t mCurrentEvent;
        bool mCurrentIsPhantom;
        std::size_t mCurrentPlane;
        bool mFirstEvent;

        PhantomPath mPath;
        Int_t mEvent;
        Double_t mX;
        Double_t mY;
        Double_t mZ;
        Double_t mEnergy;
        Int_t mIsPhantom;
        std::vector<TH2D*> mHitmaps;

        Path::ParameterSet mSimulationParameters;
    };
} // namespace corryvreckan
