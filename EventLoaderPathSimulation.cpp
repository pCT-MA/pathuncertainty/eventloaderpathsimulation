#include <cmath>

#include <TFile.h>
#include <TGraph.h>
#include <TH2.h>
#include <TTree.h>

#include "objects/Pixel.hpp"

#include "EventLoaderPathSimulation.h"

constexpr double radians(double degrees) {
    return M_PI / 180 * degrees;
}

namespace corryvreckan {
    bool IsPhantom(Int_t i); // To shut up -Wmissing-declarations
    bool IsPhantom(Int_t i) { return i == 1; }

    EventLoaderPathSimulation::EventLoaderPathSimulation(Configuration& config,
                                                         std::vector<std::shared_ptr<Detector>> detectors)
        : Module(config, detectors), mInFile(nullptr), mTracingTree(nullptr), mCurrentIndex(0), mNumberOfEntries(0),
          mCurrentEvent(0), mCurrentIsPhantom(false), mCurrentPlane(0), mFirstEvent(true) {
        mInFileName = config.getPath("infile");
        mXmlConfig = config.getPath("xml_config");

        config_.setDefault<bool>("apply_error", true);
        mApplyErrors = config.get<bool>("apply_error");
    }

    EventLoaderPathSimulation::~EventLoaderPathSimulation() {}

    void EventLoaderPathSimulation::initialize() {
        LOG(INFO) << (mApplyErrors ? "Applying errors based on spatial resolution" : "No errors are applied");

        mInFile = TFile::Open(mInFileName.c_str());
        mSimulationParameters = Path::XMLParameters(mXmlConfig);

        mTracingTree = dynamic_cast<TTree*>(mInFile->Get(gTracingTreeName));
        mNumberOfEntries = mTracingTree->GetEntries();

        mTracingTree->SetBranchAddress(gEventBranchName, &mEvent, nullptr);
        mTracingTree->SetBranchAddress(gXBranchName, &mX, nullptr);
        mTracingTree->SetBranchAddress(gYBranchName, &mY, nullptr);
        mTracingTree->SetBranchAddress(gZBranchName, &mZ, nullptr);
        mTracingTree->SetBranchAddress(gEnergyBranchName, &mEnergy, nullptr);
        mTracingTree->SetBranchAddress(gIsPhantomBranchName, &mIsPhantom, nullptr);

        mPath.Resize(mSimulationParameters.mPhantomParameters.mNumberOfLayers);
        mMeasurements.resize(mSimulationParameters.mDetectorParameters.size());

        for(std::size_t i = 0; i < get_detectors().size(); i++) {
            const auto name = "Hitmap" + std::to_string(i);
            const auto size = get_detectors().at(i)->getSize();
            mHitmaps.emplace_back(new TH2D(
                name.c_str(), name.c_str(), 400, -size.X() / 1.5, size.X() / 1.5, 400, -size.Y() / 1.5, size.Y() / 1.5));
        }
    }

    StatusCode EventLoaderPathSimulation::run(const std::shared_ptr<Clipboard>& clipboard) {
        auto setAllZero = [&]() {
            mPath.SetAllZero();
            std::fill(mMeasurements.begin(), mMeasurements.end(), Measurement{0, 0, 0});
        };

        auto binNumber = [](double size, double binSize, double value) {
            const auto shift = value + 0.5 * size;
            return static_cast<int>(shift / binSize);
        };

        auto applyError = [](double value, double error, std::default_random_engine& rng) {
            std::normal_distribution<double> errorDistribution(0, error);
            return value + errorDistribution(rng);
        };

        auto isStripType = [&](std::string const& type) {
            return type == gStripUType || type == gStripVType || type == gStripXType;
        };

        auto Column = [](double const& x, double const& y, double angle, std::string const& type) {
            if(angle == 0 || type == gStripXType)
                return x;
            if(angle == 90)
                return y;
            const auto a = (cos(radians(angle)) + sin(radians(angle) * cos(radians(90) - angle) / sin(radians(90) - angle)));
            const auto b = (sin(radians(angle)) + cos(radians(angle) * sin(radians(90) - angle) / cos(radians(90) - angle)));
            return type == gStripUType ? x / a + y / b : x / a - y / b;
        };

        if(mFirstEvent) {
            mTracingTree->GetEntry(mCurrentIndex);
            mCurrentEvent = mEvent;
            mCurrentIsPhantom = IsPhantom(mIsPhantom);
            mFirstEvent = false;
            setAllZero();
        }
        while(mNumberOfEntries > mCurrentIndex) {
            // store in phantom path and hits
            if(mCurrentIsPhantom) {
                mPath.Set(mCurrentPlane, mX, mY, mZ, mEnergy);
            } else {
                mMeasurements[mCurrentPlane][0] = mX;
                mMeasurements[mCurrentPlane][1] = mY;
                mMeasurements[mCurrentPlane][2] = mEnergy;
            }

            ++mCurrentIndex;
            ++mCurrentPlane;
            mTracingTree->GetEntry(mCurrentIndex);
            // catch the transition from detector (isPhantom = 0) to phantom (… =1)
            {
                const bool ip = IsPhantom(mIsPhantom);
                if(ip != mCurrentIsPhantom) {
                    // check for missing planes
                    if(mCurrentIsPhantom) {
                        if(mCurrentPlane != mSimulationParameters.mPhantomParameters.mNumberOfLayers) {
                            throw std::runtime_error("Number of hits and phantom size mismatch");
                        }
                    } else {
                        if(mCurrentPlane != mSimulationParameters.mDetectorParameters.size()) {
                            throw std::runtime_error("Number of hits and detectors size mismatch");
                        }
                    }

                    mCurrentPlane = 0;
                    mCurrentIsPhantom = ip;
                }
            }

            // catch the transition to the next event
            if(mCurrentEvent != mEvent) {
                clipboard->putEvent(std::make_shared<Event>(static_cast<double>(mCurrentIndex) - 0.5,
                                                            static_cast<double>(mCurrentIndex) + 0.5));

                clipboard->putData(PathVector{std::make_shared<PhantomPath>(mPath)});

                for(std::size_t i = 0; i < mMeasurements.size(); i++) {
                    const auto detectorSize = get_detectors().at(i)->getSize();
                    const auto detectorPitch = get_detectors().at(i)->getPitch();
                    const auto detectorResolution = get_detectors().at(i)->getSpatialResolution();
                    const auto angle = get_detectors().at(i)->rotation().z();
                    const auto type = get_detectors().at(i)->getType();
                    double x = mMeasurements.at(i).at(0);
                    double y = mMeasurements.at(i).at(1);
                    const auto energy = mMeasurements.at(i).at(2);

                    const auto c0 = isStripType(type) ? Column(x, y, angle, type) : x;
                    const auto col = mApplyErrors ? applyError(c0, detectorResolution.X(), mRNG) : c0;
                    const auto row = isStripType(type) ? 0 : mApplyErrors ? applyError(y, detectorResolution.Y(), mRNG) : y;

                    mHitmaps[i]->Fill(x, y);

                    clipboard->putData(
                        PixelVector{std::make_shared<Pixel>(get_detectors().at(i)->getName(),
                                                            binNumber(detectorSize.X(), detectorPitch.X(), col),
                                                            binNumber(detectorSize.Y(), detectorPitch.Y(), row),
                                                            energy,
                                                            0,
                                                            static_cast<double>(mCurrentIndex))},
                        get_detectors().at(i)->getName());
                }
                // cleanup for next event
                mCurrentEvent = mEvent;
                setAllZero();
                return StatusCode::Success;
            }
        }
        return StatusCode::EndRun;
    } // namespace corryvreckan

    void EventLoaderPathSimulation::finalize(const std::shared_ptr<ReadonlyClipboard>&) {
        for(auto hitmap : mHitmaps) {
            hitmap->Write();
            delete hitmap;
        }
        mHitmaps.clear();

        mInFile->Close();
        delete mInFile;
        mInFile = nullptr;
    }
} // namespace corryvreckan
